package com.atlassian.jwt.reader;

import com.atlassian.jwt.CanonicalHttpRequest;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

public interface JwtClaimVerifiersBuilder {
    /**
     * Encapsulate the building of requirements that we place upon JWTs in incoming requests.
     *
     * @param request incoming request
     * @return {@link Map} of claim name to verifier for claims upon which we place requirements
     * @throws UnsupportedEncodingException if {@link java.net.URLEncoder} cannot encode the request's characters
     * @throws NoSuchAlgorithmException     if the hashing algorithm does not exist at runtime
     */
    Map<String, ? extends JwtClaimVerifier> build(CanonicalHttpRequest request) throws UnsupportedEncodingException, NoSuchAlgorithmException;
}
