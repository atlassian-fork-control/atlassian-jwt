package com.atlassian.jwt.exception;

/**
 * Thrown if a malformed secret key is passed to verify a JWT (e.g. with insufficient length).
 *
 * @since 2.0.0
 */
public class JwtMalformedSharedSecretException extends RuntimeException
{
    public JwtMalformedSharedSecretException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
