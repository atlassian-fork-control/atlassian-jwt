# Atlassian JWT (Server version)

Atlassian JWT is a library for authenticating requests containing JWTs (JSON Web Tokens) in the query string or Authorization header. It is based on the [11th draft of the JSON Web Token IETF memo](http://tools.ietf.org/html/draft-ietf-oauth-json-web-token-11) by the OAuth Working Group.

See [the JWT draft](http://tools.ietf.org/html/draft-ietf-oauth-json-web-token-11#section-3.1) for more information on making JWT authenticated requests.

**Note to Atlassian developers:** This is the Atlassian Server version of the library. As of February 2019, Cloud products still use the library [but intend to fork when changes are required](https://bitbucket.org/atlassian/atlassian-jwt/pull-requests/81/issue-jwt-27-bump-dependency-versions/diff#comment-78717271).

# How do I..

## Build atlassian-jwt

Run ``mvn clean install`` from the root directory. (Append ``-DskipTests`` if you want to skip the tests.)

The automated builds are located here: https://deployment-bamboo.internal.atlassian.com/chain/viewChain.action?planKey=JWT-JWTM

## Run atlassian-jwt in an Atlassian product

**Build atlassian-jwt**, install the [Atlassian SDK](https://developer.atlassian.com/display/DOCS/Getting+Started), then run ``atlas-debug -Dproduct=jira`` from the ``/plugin`` directory.

# Project modules

## jwt-api

The collection of interfaces and utility classes that form the stable API of atlassian-jwt.

## jwt-core

The reference implementation of jwt-api.

## jwt-applinks

A set of services for issuing and verifying JWTs to/from applications linked via [application-links](https://bitbucket.org/atlassian/application-links).

## jwt-plugin

An Atlassian plugin that bundles jwt-api, jwt-core and jwt-applinks modules.

## jwt-test-plugin

An Atlassian plugin that is deployed alongside jwt-test-plugin to expose test functionality needed by the jwt-integration-tests module. 

This module should **never** be deployed in production.

## jwt-integration-tests

A light-weight Jetty application and set of test classes that run against an Atlassian application. 

# Supported Signing Algorithms

Atlassian JWT supports the following JWA signing algorithms:

- HMAC SHA-256 ("HS256")

# Deployment environments

## Atlassian products

Atlassian products should add the jwt-plugin as a bundled plugin. 

Optionally, an Atlassian product may include the jwt-api, jwt-core and jwt-applinks libraries in their WEB-INF/lib directory and look up the implementations provided by jwt-plugin via OSGi. In this way JWT services may be used from the product core. 

## 3rd party applications

Since atlassian-applinks is non-trivial to implement, 3rd party applications should embed only the jwt-api and jwt-core libraries. The integration-tests module contains an example Jetty application that embed jwt-api and jwt-core as a reference.

# Upgrade notes

### 3.1.0

Added a new SPI, `JwtIssuerClaimVerifiersRegistry` that allows issuers to provide their own claim verifiers. The default still verifies the `qsh` claim but issuers are now able to verify custom claims.  

### 3.0.0

Upgraded to Atlassian's Java platform 5 in order to support Java 11.

### 2.0.0

This version switched to Nimbus JOSE JWT 4.x which among other changes has enforced requirements on the minimal secret key length of 256 bits.

# Builds

Build and release plan is located at https://ecosystem-bamboo.internal.atlassian.com/browse/ATLASJWT-ATLASJWTM